package org.va.model

/**
 * This class defines the Gender options supported by this application.
 */
enum Gender {
    MALE("Man"),
    FEMALE("Vrouw"),
    UNKNOWN("Onbekend")

    String value

    Gender(String value) {
        this.value = value
    }

    String toString() { return value }
    String getKey() { return name() }
}
