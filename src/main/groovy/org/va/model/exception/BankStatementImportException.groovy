package org.va.model.exception

/**
 * This exception indicates that there was a problem with importing the bank statement file. This exception has a dutch message
 * that is suitable for direct display on the screen.
 */
class BankStatementImportException extends RuntimeException {

    BankStatementImportException(String s) {
        super(s)
    }
}
