package org.va.model

/**
 * This enum defines the roles a person can have within the organisation. This role is for longer than one event.
 */
public enum PersonRole {

    REGULAR ("-"),
    MANAGEMENT ("Bestuur"),
    HONORARY ("Erelid")

    String description

    PersonRole(String description) {
        this.description = description
    }

    String toString() { return description }
    String getKey() { return name() }

}