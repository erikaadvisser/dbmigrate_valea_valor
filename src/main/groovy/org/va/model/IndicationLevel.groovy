package org.va.model

/**
 * This enum defines how good something is.
 */
enum IndicationLevel {

    OK(true),
    WARNING(true),
    ERROR(false)

    boolean allowPurchase

    IndicationLevel(boolean allowPurchase) {
        this.allowPurchase = allowPurchase
    }
}
