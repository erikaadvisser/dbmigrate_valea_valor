package org.va.model

/**
 * This class defines the different types a Transaction can have.
 */
enum PaymentType {
    BANK("Bank"),
    CASH("Cash"),
    PIN("Pin"),
    IDEAL("iDEAL")

    String description

    PaymentType(String description) {
        this.description = description
    }

    String toString() { return description }
    String getKey() { return name() }
}
