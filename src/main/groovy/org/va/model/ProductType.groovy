package org.va.model

/**
 * This class defines the types of products the system understands.
 */
enum ProductType {
    ENTRY_MAIN_EVENT("Toegang"),
    ENTRY_SIDE_EVENT("Toegang side-event"),
    MEMBERSHIP("Lidmaatschap"),
    MERCHANDISE("Merchandise"),
    FACILITIES("Faciliteiten"),
    CATERING("Catering"),
    OTHER("Anders")

    String description

    ProductType(String description) {
        this.description = description
    }

    String toString() {
        return description
    }

    String getKey() {
        return name()
    }
}
