package org.va.valor

import org.joda.time.LocalDateTime

/**
 * A memo is a remark that can be made for a person. It can be marked as processed to make it less important.
 */
class Memo {

    long id;
    String text;
    boolean processed = false
    Person target
    Person enteredBy
    LocalDateTime timeStamp = new LocalDateTime()

}
