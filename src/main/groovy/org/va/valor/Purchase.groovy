package org.va.valor

import org.joda.time.LocalDate

/**
 * A purchase is the act of buying a product (or receiving a product if it's free).
 */
class Purchase {

    Long id
    Product product
    BigDecimal amount
    BigDecimal amountOpen
    Person person
    LocalDate purchaseDate

    // For migration purposes
    long valeaid

    static constraints = {
    }
}
