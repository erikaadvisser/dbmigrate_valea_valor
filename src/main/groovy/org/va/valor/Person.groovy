package org.va.valor

import org.joda.time.LocalDate
import org.joda.time.Period
import org.va.model.Gender
import org.va.model.PersonRole

/**
 * A person is someone who participates in VA events.
 */
class Person {

    long id

    // For migration purposes
    long valeaid

    String plin
    boolean member

    String firstName
    String infix
    String lastName

    LocalDate dateOfBirth

    Gender gender = Gender.UNKNOWN

    String street
    String postCode
    String city
    String country
    String phone

    String email
    String iban

    PersonRole role = PersonRole.REGULAR


    static constraints = {
        plin(nullable: true)
        infix(nullable: true)
        dateOfBirth(nullable: true)
        gender(nullable: true)
        street(nullable: true)
        postCode(nullable: true)
        city(nullable: true)
        country(nullable: true)
        phone(nullable: true)
        email(nullable: true)
        iban(nullable: true)
    }

    @Override
    public String toString() {
        return "{$plin} ${firstName} ${lastName}"
    }

    public String fullName() {
        String middle = (infix == null) ? "" : infix + " "
        String first = (firstName == null) ? "?" : firstName
        String last = (lastName == null) ? "?" : lastName

        return first + " " + middle + last
    }

    public String shortname() {
        String name = firstName + " " + lastName?.substring(0,1);
        if ( name.length() > 10) {
            return name.substring(0, 10)
        }
        return name
    }

    public String dateOfBirthString() {
        if (dateOfBirth == null) {
            return ""
        }
        return dateOfBirth.toString("dd-MM-yyyy")
    }

    public Integer age() {
        LocalDate now = new LocalDate()
        return ageAt(now)
    }

    public Integer ageAt(LocalDate date) {
        if (dateOfBirth == null) {
            return null
        }
        Period period = new Period(dateOfBirth, date)
        return period.years
    }

    String displayPlin() {
        return formatPlin(this.plin)
    }

    public static String formatPlin(String plinInput) {
        if (plinInput == null) {
            return "?"
        }
        if (plinInput.isInteger()) {
            int numberPlin = plinInput as int
            return String.format("%04d", numberPlin)
        }
        return plinInput
    }
}
