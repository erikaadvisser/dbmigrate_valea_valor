package org.va.valor

import org.joda.time.LocalDate
import org.va.model.ProductType
import org.va.model.Tariff

/**
 * A product is something a person can buy (or get if it's free). Some products allow entry to events, other
 * products provide a form of membership. Finally there are products (like t-shirts) that do not relate to
 * event participation but can be bought none the less.
 *
 * Membership products are valid for year that is part of their saleEnd variable.
 */
class Product {

    long id
    String name
    Event event
    BigDecimal price

    /** Last date for selling this product. */
    LocalDate saleEnd
    Tariff tariff
    ProductType type

    /** Used for year membership and year package type products */
    Integer year

    static constraints = {
        event(nullable: true)
        year(nullable: true)
    }

    String description() {
        if (event) {
            if (type == ProductType.ENTRY_MAIN_EVENT) {
                return "${name} ${event.name} (${tariff.description})"
            }
            return "${name} ${event.name}"
        }
        return "${name}"
    }

}
