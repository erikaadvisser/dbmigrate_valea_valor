package org.va.valor

import org.joda.time.LocalDateTime
import org.va.model.ParticipationRole
import org.va.model.Tariff

/**
 * A participation is the fact that a person is going to participate (or has participated) in a VA event.
 */
class Participation {

    long id

    // For migration purposes
    long valeaid


    Person person
    Event event
    ParticipationRole participationRole

    LocalDateTime checkIn
    LocalDateTime checkOut

    /** This flag indicates that the person actually participated, not just bought a ticket for it. Only set to true during event itself. */
    boolean participatesInSideEvent = false

    Tariff tariff = Tariff.FULL

    Purchase entryMainPurchase
    Purchase entrySidePurchase
    Purchase membershipPurchase

    static constraints = {
        entryMainPurchase(nullable: true)
        membershipPurchase(nullable: true)
        entrySidePurchase(nullable: true)
        checkIn(nullable: true)
        checkOut(nullable: true)
        participationRole(nullable: true)
    }
}
