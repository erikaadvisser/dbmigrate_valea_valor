package org.va.valor

import org.joda.time.LocalDate

/**
 * And event is a VA event.
 */
class Event {

    long id
    String name
    LocalDate start
    LocalDate end

    static constraints = {
    }

    String toString() {
        return name
    }

    static List<Event> findAllByYear(int year) {
        LocalDate yearStart = new LocalDate(year, 1, 1);
        LocalDate nextYearStart = yearStart.plusYears(1)

        return Event.findAll({start >= yearStart && start < nextYearStart })
    }
}
