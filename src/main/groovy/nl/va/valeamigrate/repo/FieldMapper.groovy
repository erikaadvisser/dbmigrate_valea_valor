package nl.va.valeamigrate.repo

import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime

import java.lang.reflect.Field

/**
 * This class examines a Domain class and returns representations useful for database storage
 */
class FieldMapper {

    static def ignoreList = [ "id", "metaClass", "constraints", "mapping", "\$staticClassInfo", "\$callSiteArray", "__\$stMC" ]

    static def columList(Class clazz) {
        def part = ""
        clazz.getDeclaredFields().each({
            part += processFieldForName(it)
        })

        return part + "version"
    }

    static def processFieldForName(Field field) {
        String name = field.name;
        if (ignoreList.contains(name)) {
            return ""
        }

        if ( field.type.name.startsWith("org.va.valor")) {
            name = name + "Id"
        }

        def databaseName = camelCaseToUnderscore(name)
        return databaseName + ","
    }

    static String camelCaseToUnderscore(String input) {
        def builder = new StringBuilder()
        input.chars.each({builder.append(singleCharCamelCaseToUnderscore(it))})
        return builder.toString()
    }

    static String singleCharCamelCaseToUnderscore(Character c) {
        if ( c.isUpperCase() ) {
            return "_${c.toLowerCase()}"
        }
        return "${c}"
    }

    static def valueList(Object object) {
        Class clazz = object.class

        def part = ""
        clazz.getDeclaredFields().each({
            part += processFieldForValue(object, it)
        })
        return part + "0"
    }

    static String processFieldForValue(Object object, Field field) {
        String name = field.name;

        if (ignoreList.contains(name)) {
            return ""
        }

        def value = object[field.name]
        if ( value == null) {
            return "null,"
        }

        def typeName = field.type.name
        if (typeName == "boolean" || typeName == "long") {
            return "${value},"
        }
        if (typeName == "java.lang.String") {
            String trimmed = value.trim();
            String escapedValue = escapeString(trimmed);
            return "'${escapedValue}',"
        }
        if (typeName == "org.joda.time.LocalDate" ) {
            LocalDate date = object[field.name]
            return "'${date?.toString("yyyy-MM-dd")}',"
        }

        if (typeName == "org.joda.time.LocalTime" ) {
            LocalTime time = object[field.name]
            return "'${time?.toString("hh:mm:ss")}',"
        }

        if (typeName == "org.joda.time.LocalDateTime" ) {
            LocalDateTime dateTime = object[field.name]
            return "'${dateTime?.toString("yyyy-MM-dd hh:mm:ss")}',"
        }

        if (typeName.startsWith("org.va.valor")) {
            long id = value.id
            return "${id},"
        }
        if (typeName.startsWith("org.va.model")) {
            String enumName = value.name()
            return "'${enumName}',"
        }

        if (typeName == "java.math.BigDecimal") {
            return "${value},"
        }

        throw new RuntimeException("Encountered unmapped column type: ${typeName} for ${object.class}.${field.name}")
    }

    static String escapeString(String input) {
        String result = ""
        input.chars.each({result += escape(it)})
        return result
    }

    static String escape(char c) {
        if ( c == '\\') {
            return "\\\\"
        }
        if ( c == '\'') {
            return "\\'"
        }
        if ( c == '"') {
            return "\\\""
        }
        if ( c == "\t") {
            return " "
        }
        return "${c}"
    }
}
