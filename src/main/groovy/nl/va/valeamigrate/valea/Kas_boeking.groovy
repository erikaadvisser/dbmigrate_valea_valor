package nl.va.valeamigrate.valea

import nl.va.valeamigrate.StaticContext
import nl.va.valeamigrate.dbutil.DateMapper
import org.joda.time.LocalDate
import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class represents a Kasboeking row in Valea.
 */
class Kas_boeking {

    long id
    Long bank_entry_id
    BigDecimal bedrag
    String naam_tegenrekening
    LocalDate datum
    String kassa
    Integer bladnr
    String omschrijving1
    String omschrijving2
    String omschrijving3

    String toString() {
        return "[Kasboeking date=${datum}, ${bedrag}, ${omschrijving1} ${omschrijving2} ${omschrijving3}]"
    }

    /**
     * The where clause " datum >= '2013-01-01' and datum < '2016-01-01' " is used because there exist double bookings that have a booking date > 2015.
     */
    static List<Integer> listIdsfrom2013Onwards() {
        return StaticContext.valeaTemplate.queryForList("select id from kasboeking where datum >= '2013-01-01' and datum < '2016-01-01' order by id ", Integer)
    }

    static Kas_boeking findById(long id) {
        String sql = "SELECT * FROM kasboeking WHERE id = ?";
        try {
            Kas_boeking boeking = StaticContext.valeaTemplate.queryForObject(
                    sql,
                    new RowMapper<Kas_boeking>() {
                        @Override
                        public Kas_boeking mapRow(ResultSet rs, int row) throws SQLException {
                            Kas_boeking record = new Kas_boeking()
                            record.id = rs.getLong("id")
                            record.bank_entry_id = rs.getLong("bank_entry_id")
                            record.bedrag = rs.getBigDecimal("bedrag")
                            record.naam_tegenrekening = rs.getString("naam_tegenrekening")
                            record.datum = DateMapper.toLocalDate(rs, "datum")
                            record.kassa = rs.getString("kassa")
                            record.bladnr = rs.getString("bladnr")
                            record.omschrijving1 = rs.getString("omschrijving1")
                            record.omschrijving2 = rs.getString("omschrijving2")
                            record.omschrijving3 = rs.getString("omschrijving3")
                            return record
                        }
                    },
                    id)

            return boeking
        }
        catch(Exception e) {
            throw new RuntimeException("Failed to retrieve kasboeking with id=${id}, due to ${e.getMessage()}.", e)
        }
    }


}
