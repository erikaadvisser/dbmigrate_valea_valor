package nl.va.valeamigrate.valea

import nl.va.valeamigrate.StaticContext
import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class defines a mapping from a boeking to an event
 */
class ValeaBoekingMapping {

    public long eventId;
    public long boekingId;
    public BigDecimal bedrag;
    public String betaald_voor;
    public String product;
    public String omschrijving;

    static RowMapper rowMapper = new RowMapper<ValeaBoekingMapping>() {
        @Override
        public ValeaBoekingMapping mapRow(ResultSet rs, int row) throws SQLException {
            ValeaBoekingMapping info = new ValeaBoekingMapping()
            info.eventId = rs.getLong("event_id")
            info.boekingId = rs.getLong("boeking_id")
            info.bedrag = rs.getBigDecimal("bedrag")
            info.betaald_voor = rs.getString("betaald_voor")
            info.product = rs.getString("product")
            info.omschrijving = rs.getString("omschrijving")

            return info
        }
    }

    static ValeaBoekingMapping findById(int id) {
        String sql = "SELECT * FROM opmerkingen WHERE id = ?";
        ValeaBoekingMapping record = StaticContext.valeaTemplate.queryForObject(
                sql,rowMapper, id);
        return record;
    }

    static List<ValeaBoekingMapping> findByKasBoekingId(long id) {
        def sql = "SELECT * from boeking_event where boeking_id=?"
        Object[] args = [id]
        return StaticContext.valeaTemplate.query(sql, args, rowMapper)
    }
}
