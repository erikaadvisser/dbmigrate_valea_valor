package nl.va.valeamigrate

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.steps.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.valor.*

/**
 * This class is the Spring starting point for the migration, coordinating the entire operation.
 */
@Component
class MigrationManager {

    def static log = LoggerFactory.getLogger(MigrationManager)

    @Autowired ValorRepo valorRepo

    @Autowired DeelnemersMigrator deelnemersMigrator
    @Autowired PrijzenMigrator prijzenMigrator
    @Autowired OpmerkingenMigrator opmerkingenMigrator
    @Autowired KasboekingMigrator kasboekingMigrator
    @Autowired EventsMigrator eventsMigrator
    @Autowired BoekingEventsMigrator boekingEventsMigrator


    void run() {
        long startTime = System.currentTimeMillis()

        boolean all = false

        clearOldData(all)
        fixInvalidValeaData()
        prepareDatabase()
        prepareClasses()


        if (all) {
            deelnemersMigrator.run()
            opmerkingenMigrator.run()
            prijzenMigrator.run()
            kasboekingMigrator.run()
            eventsMigrator.run()
            boekingEventsMigrator.run()
        }
        else {
//        deelnemersMigrator.run()
//        opmerkingenMigrator.run()
            prijzenMigrator.run()
            kasboekingMigrator.run()
            eventsMigrator.run()
            boekingEventsMigrator.run()
        }


        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Total duration: ${migrateTime} s")
    }

    def prepareDatabase() {
        valorRepo.runSql("ALTER TABLE ${Person.class.simpleName} AUTO_INCREMENT = 10000")
    }

    def fixInvalidValeaData() {
        StaticContext.valeaTemplate.execute("update deelnemers set plin='12' where plin='0'")
    }

    def clearOldData(boolean all) {
//        valorRepo.runSql("drop database valor")
//        valorRepo.runSql("drop database valor")

        if (all) {
            valorRepo.runSql("delete from payment_match")
            valorRepo.runSql("delete from participation")
            valorRepo.runSql("delete from purchase")
            valorRepo.runSql("delete from payment")
            valorRepo.runSql("delete from product")
            valorRepo.runSql("delete from event")

            valorRepo.runSql("delete from membership")
            valorRepo.runSql("delete from memo")
            valorRepo.runSql("delete from user")
            valorRepo.runSql("delete from person")
        }
        else {
            valorRepo.runSql("delete from payment_match")
            valorRepo.runSql("delete from participation")
            valorRepo.runSql("delete from purchase")
            valorRepo.runSql("delete from payment")
            valorRepo.runSql("delete from product")
            valorRepo.runSql("delete from event")

//        valorRepo.runSql("delete from membership")
//        valorRepo.runSql("delete from memo")
//        valorRepo.runSql("delete from user")
//        valorRepo.runSql("delete from person")

        }
    }

    def prepareClasses() {
        addSaveMethod(Event)
        addSaveMethod(Membership)
        addSaveMethod(Memo)
        addSaveMethod(Participation)
        addSaveMethod(Person)
        addSaveMethod(Product)
        addSaveMethod(Purchase)
        addSaveMethod(Payment)
        addSaveMethod(PaymentMatch)
        addSaveMethod(User)

        addDeleteMethod(Person)
    }

    def addSaveMethod(Class clazz) {
        clazz.metaClass.save = { -> return StaticContext.saveEntity(delegate) }
    }

    def addDeleteMethod(Class clazz) {
        clazz.metaClass.delete = { -> return StaticContext.deleteEntity(delegate) }
    }
}
