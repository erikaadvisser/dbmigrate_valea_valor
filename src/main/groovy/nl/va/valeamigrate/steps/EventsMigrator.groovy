package nl.va.valeamigrate.steps

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.valea.ValeaEvent
import org.joda.time.LocalDate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.valor.*

/**
 * This class migrates Valea Events to Valor Participations
 */
@Component
class EventsMigrator {

    def static log = LoggerFactory.getLogger(EventsMigrator)

    @Autowired ValorRepo valorRepo

    int eventsMigrated = 0
    int participationsCreated = 0
    int purchasesCreated = 0
    int skipped = 0
    def eventCache = [:]
    def productCache = [:]

    void run() {
        long startTime = System.currentTimeMillis()

        migrateByYear(2013)
        migrateByYear(2014)

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000
        log.info("Skipped ${skipped} events.")
        log.info("duration: ${migrateTime} s")
    }

    private void migrateByYear(int year) {
        eventsMigrated = 0
        participationsCreated = 0
        purchasesCreated = 0

        List<Long> ids = ValeaEvent.findAllByYear(year)

        log.info("Migrating ${ids.size()} events for ${year}.")
        ids.each({ migrate(it) })

        log.info(" Created ${participationsCreated} participations for ${year}.")
        log.info(" Created ${purchasesCreated} membership purchases for ${year}.")
    }

    void migrate(long id) {
        ValeaEvent valeaEvent = ValeaEvent.findById(id)

        Person targetPerson = findTargetPerson(valeaEvent)
        if (!targetPerson) {
            log.warn("Skipped migration of event ${valeaEvent.id} because target person was not migrated, deelnemer.id=${valeaEvent.deelnemer_id}")
            skipped += 1
            return
        }

        if (isMembershipEvent(valeaEvent)) {
            Purchase purchase = createMembershipContributionPurchase(valeaEvent, targetPerson)
            purchase.save()
            purchasesCreated += 1
        }
        else {
            Participation participation = createParticipation(valeaEvent, targetPerson)
            participation.save()
            participationsCreated += 1

            Purchase purchase = createPurchaseForParticipation(participation, targetPerson, valeaEvent)
            purchase.save()
        }
        eventsMigrated += 1
    }

    Purchase createPurchaseForParticipation(Participation participation, Person person, ValeaEvent valeaEvent) {
        Purchase purchase = new Purchase()

        purchase.valeaid = valeaEvent.id
        purchase.person = person
        purchase.amount = 0
        purchase.amountOpen = 0
        purchase.purchaseDate = createPurchaseDate(valeaEvent)
        purchase.product = findTempProduct()

        return purchase
    }

    Purchase createMembershipContributionPurchase(ValeaEvent valeaEvent, Person targetPerson) {
        Product membershipProduct = findMembershipProduct(valeaEvent)
        Purchase purchase = new Purchase()

        purchase.valeaid = valeaEvent.id
        purchase.person = targetPerson
        purchase.amount = membershipProduct.price
        purchase.amountOpen = membershipProduct.price
        purchase.purchaseDate = createPurchaseDate(valeaEvent)
        purchase.product = membershipProduct

        return purchase
    }

    Product findMembershipProduct(ValeaEvent valeaEvent) {
        String productName = "Verenigingslidmaatschap ${valeaEvent.jaar}"
        return findProductByName(productName)
    }

    Product findTempProduct() {
        return findProductByName("temp-product")
    }

    private Object findProductByName(String productName) {
        if (!productCache.containsKey(productName)) {
            Product membershipProduct = valorRepo.getMinimalProductByName(productName)
            productCache.put(productName, membershipProduct)
        }
        return productCache.get(productName)
    }

    LocalDate createPurchaseDate(ValeaEvent valeaEvent) {
        int jaar = valeaEvent.jaar
        LocalDate date = new LocalDate(jaar, 1, 1)
        return date
    }

    Participation createParticipation(ValeaEvent valeaEvent, Person targetPerson ) {
        Participation participation = new Participation()
        participation.person = targetPerson
        participation.event = findTargetEvent(valeaEvent)
        participation.checkIn = valeaEvent.incheck_date
        participation.checkOut = valeaEvent.uitcheck_date
        participation.tariff = valeaEvent.getTariff()
        participation.valeaid = valeaEvent.id

        return participation
    }

    static boolean isMembershipEvent(ValeaEvent valeaEvent) {
        return valeaEvent.naam == 0
    }

    Event findTargetEvent(ValeaEvent valeaEvent) {
        String eventNameString = findEventNameFromNumber(valeaEvent)
        String valorEventNaam = "${eventNameString} ${valeaEvent.jaar}"

        if (!eventCache.containsKey(valorEventNaam)) {
            Event event = valorRepo.getIdOnlyEventByName(valorEventNaam)
            eventCache.put(valorEventNaam,event)
        }
        return eventCache.get(valorEventNaam)
    }

    String findEventNameFromNumber(ValeaEvent valeaEvent) {
        switch(valeaEvent.naam) {
            case 1: return "Sumdag"
            case 2: return "Moots 1"
            case 3: return "Summoning"
            case 4: return "Moots 2"
            default: throw new IllegalArgumentException("Cannot translate event.name=${valeaEvent.naam} for event.id=${valeaEvent.id}")
        }
    }

    Person findTargetPerson(ValeaEvent valeaEvent) {
        long deelnemerId = valeaEvent.deelnemer_id
        if (!valorRepo.deelnemerWithIdIsMigrated(deelnemerId)) {
            return null
        }
        long personId = valorRepo.getPersonIdByValeaId(deelnemerId)
        return new Person(id: personId)
    }
}
