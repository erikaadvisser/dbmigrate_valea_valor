package nl.va.valeamigrate.steps

import org.joda.time.LocalDate
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.va.model.ProductType
import org.va.model.Tariff
import org.va.valor.Event
import org.va.valor.Product

/**
 * This class migrates the prijzen.
 *
 * The jaarprijzen are not relevant for migration: this data is fixed as far as the migration is concerned since we only want to migrate 2013 and 2014.
 *
 *<pre>
 * 2009	5	15	150	95	2009-05-01	40	30
 * 2010	5	15	150	95	2010-05-01	30	30
 * 2011	5	15	150	95	2011-05-01	30	30
 * 2012	5	15	165	130	2012-03-30	40	40
 * 2013	5	15	165	130	2013-04-01	40	40
 * 2014	5	15	165	130	<Error>	40	40
 * </pre>
 *
 * The evenementsprijzen also are fixed for our migration, for 2013-2014.
 *
 */
@Component
class PrijzenMigrator {

    def static log = LoggerFactory.getLogger(PrijzenMigrator)

    void run() {
        long startTime = System.currentTimeMillis()

        log.info("Creating products 2013 and 2014")

        createEventsAndProducts()

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("duration: ${migrateTime} s")
    }

    def createEventsAndProducts() {
        createJaarProducts(2013)
        createJaarProducts(2014)
        createJaarProducts(2015)

        Event sumdag_2013 = new Event(name: 'Sumdag 2013', start: new LocalDate('2013-03-23'), end: new LocalDate('2013-03-23')).save()
        Event m1_2013 = new Event(name: 'Moots 1 2013', start: new LocalDate('2013-04-26'), end: new LocalDate('2013-04-28')).save()
        Event summoning_2013 = new Event(name: 'Summoning 2013', start: new LocalDate('2013-08-23'), end: new LocalDate('2013-08-26')).save()
        Event m2_2013 = new Event(name: 'Moots 2 2013', start: new LocalDate('2013-10-11'), end: new LocalDate('2013-10-13')).save()

        createProductsSumDag(sumdag_2013)
        createProductsMoots(m1_2013, new LocalDate('2013-04-11'))
        createProductsSummoning( summoning_2013, new LocalDate('2013-08-09'))
        createProductsMoots(m2_2013, new LocalDate('2013-09-27'))


        Event sumdag_2014 = new Event(name: 'Sumdag 2014', start: new LocalDate('2014-03-15'), end: new LocalDate('2014-03-15')).save()
        Event m1_2014 = new Event(name: 'Moots 1 2014', start: new LocalDate('2014-05-16'), end: new LocalDate('2014-05-18')).save()
        Event summoning_2014 = new Event(name: 'Summoning 2014', start: new LocalDate('2014-08-15'), end: new LocalDate('2014-08-18')).save()
        Event m2_2014 = new Event(name: 'Moots 2 2014', start: new LocalDate('2014-10-10'), end: new LocalDate('2014-10-12')).save()

        createProductsSumDag(sumdag_2014)
        createProductsMoots(m1_2014, new LocalDate('2014-05-04'))
        createProductsSummoning( summoning_2014, new LocalDate('2014-07-31'))
        createProductsMoots(m2_2014, new LocalDate('2014-09-26'))

        Event sumdag_2015 = new Event(name: 'Sumdag 2015', start: new LocalDate('2014-02-28'), end: new LocalDate('2015-02-28')).save()
        Event m1_2015 = new Event(name: 'Moots 1 2015', start: new LocalDate('2015-04-10'), end: new LocalDate('2015-04-12')).save()
        Event summoning_2015 = new Event(name: 'Summoning 2015', start: new LocalDate('2015-07-17'), end: new LocalDate('2015-07-20')).save()
        Event m2_2015 = new Event(name: 'Moots 2 2015', start: new LocalDate('2015-10-19'), end: new LocalDate('2014-10-11')).save()

        new Product(event: null, name: "temp-product", price: 0.0, saleEnd: new LocalDate('2001-01-01'), tariff: Tariff.FULL, type: ProductType.OTHER).save()

    }

    def createProductsSumDag(Event event) {
        new Product(event: event, name: "Poortinschrijving", price: 10.0, saleEnd: event.end, tariff: Tariff.FULL, type: ProductType.ENTRY_MAIN_EVENT).save()
        new Product(event: event, name: "Inschrijving", price: 0.0, saleEnd: event.end, tariff: Tariff.FREE, type: ProductType.ENTRY_MAIN_EVENT).save()
    }

    def createProductsMoots(Event event, LocalDate voorinschrijfDeadLine) {
        createProducts(event, voorinschrijfDeadLine,
                [
                        (Tariff.FULL): 55.00,
                        (Tariff.CREW): 40.00,
                        (Tariff.MINOR): 27.50],
                [
                        (Tariff.FULL): 65.00,
                        (Tariff.CREW): 45.00,
                        (Tariff.MINOR): 32.50])
    }

    def createProductsSummoning(Event event, LocalDate voorinschrijvingsDeadline) {
        createProducts(event, voorinschrijvingsDeadline,
                [
                        (Tariff.FULL): 75.00,
                        (Tariff.CREW): 50.00,
                        (Tariff.MINOR): 37.50],
                [
                        (Tariff.FULL): 90.00,
                        (Tariff.CREW): 55.00,
                        (Tariff.MINOR): 45.00])
    }


    def createProducts(Event event, LocalDate voorinschrijfDeadLine, def voorinschrijfPrices, def poortPrices) {
        def priceBank = (event.start.year == 2014) ? 5: 2.5
        
        new Product(event: event, name: "Voorinschrijving", price: voorinschrijfPrices[Tariff.FULL], saleEnd: voorinschrijfDeadLine, tariff: Tariff.FULL, type: ProductType.ENTRY_MAIN_EVENT).save()
        new Product(event: event, name: "Voorinschrijving", price: voorinschrijfPrices[Tariff.CREW], saleEnd: voorinschrijfDeadLine, tariff: Tariff.CREW, type: ProductType.ENTRY_MAIN_EVENT).save()
        new Product(event: event, name: "Voorinschrijving", price: voorinschrijfPrices[Tariff.MINOR], saleEnd: voorinschrijfDeadLine, tariff: Tariff.MINOR, type: ProductType.ENTRY_MAIN_EVENT).save()

        new Product(event: event, name: "Poortinschrijving", price: poortPrices[Tariff.FULL], saleEnd: event.end, tariff: Tariff.FULL, type: ProductType.ENTRY_MAIN_EVENT).save()
        new Product(event: event, name: "Poortinschrijving", price: poortPrices[Tariff.CREW], saleEnd: event.end, tariff: Tariff.CREW, type: ProductType.ENTRY_MAIN_EVENT).save()
        new Product(event: event, name: "Poortinschrijving", price: poortPrices[Tariff.MINOR], saleEnd: event.end, tariff: Tariff.MINOR, type: ProductType.ENTRY_MAIN_EVENT).save()

        new Product(event: event, name: "Inschrijving", price: 0.0, saleEnd: event.end, tariff: Tariff.FREE, type: ProductType.ENTRY_MAIN_EVENT).save()

        new Product(event: event, name: "Tafel", price: 5.0, saleEnd: event.end, tariff: Tariff.FULL, type: ProductType.OTHER).save()
        new Product(event: event, name: "Bank", price: priceBank, saleEnd: event.end, tariff: Tariff.FULL, type: ProductType.OTHER).save()

        new Product(event: event, name: "Evenementslidmaatschap", price: 5.0, saleEnd: event.end, tariff: Tariff.FULL, type: ProductType.MEMBERSHIP).save()
    }

    def createJaarProducts(int year) {
        new Product(event: null, name: "Verenigingslidmaatschap ${year}".toString(), price: 15.00, saleEnd: new LocalDate("${year}-12-31".toString()), tariff: Tariff.FULL, type: ProductType.MEMBERSHIP).save()
        new Product(event: null, name: "Gratis lidmaatschap ${year}".toString(), price: 0.00, saleEnd: new LocalDate("${year}-12-31".toString()), tariff: Tariff.FREE, type: ProductType.MEMBERSHIP).save()
        new Product(event: null, name: "T-shirt ${year}".toString(), price: 15.00, saleEnd: new LocalDate("${year}-12-31".toString()), tariff: Tariff.FULL, type: ProductType.MERCHANDISE).save()
    }

}
