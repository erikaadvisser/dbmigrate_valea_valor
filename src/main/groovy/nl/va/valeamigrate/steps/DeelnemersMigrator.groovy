package nl.va.valeamigrate.steps

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.valea.Deelnemer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.model.Gender
import org.va.model.PersonRole
import org.va.valor.Membership
import org.va.valor.Person

/**
 * This class migrates the deelnemers.
 */
@Component
class DeelnemersMigrator {

    def static log = LoggerFactory.getLogger(DeelnemersMigrator)

    @Autowired ValorRepo valorRepo

    int personMigrated = 0
    int skipped = 0
    int membershipMigrated = 0

    void run() {
        long startTime = System.currentTimeMillis()
        List<Integer> ids = Deelnemer.listIds()

        log.info("Migrating ${ids.size()} persons.")
        ids.each({migrate(it)})

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Migrated ${personMigrated} persons.")
        log.info("Migrated ${membershipMigrated} memberships.")
        log.info(" Skipped ${skipped} persons.")
        log.info("duration: ${migrateTime} s")
    }

    boolean migrate(int id) {
        Deelnemer deelnemer = Deelnemer.findById(id)

        if (wantToMigrate(deelnemer)) {
            def person = createPerson(deelnemer)
            savePersonForcingIdToPlin(person)
            personMigrated += 1

            def membership = createMembership(deelnemer, person)
            if (membership) {
                membership.save()
                membershipMigrated += 1
            }
        }
        else {
            log.debug("Not migrating: ${deelnemer}")
            skipped += 1
        }

        if (personMigrated % 500 == 0 && personMigrated > 0) {
            log.info("at ${personMigrated}")
        }

    }

    def savePersonForcingIdToPlin(Person person) {
        int plin = Integer.parseInt(person.plin)
        person.save()
        valorRepo.runSqlUpdate("update ${Person.class.simpleName} set id=? where id=?", plin, person.id)
        person.id = plin
    }

    Person createPerson(Deelnemer deelnemer) {
        Person person = new Person()

        person.valeaid = deelnemer.id
        person.plin = deelnemer.plin
        person.firstName = deelnemer.voornaam
        person.infix = deelnemer.tussenvoegsels
        person.lastName = deelnemer.achternaam
        person.dateOfBirth = deelnemer.geboortedatum

        person.street = deelnemer.straat_huisnr
        person.postCode = deelnemer.postcode
        person.city = deelnemer.woonplaats
        person.country = deelnemer.land

        person.email = deelnemer.email_adres
        person.phone = deelnemer.telefoonnummers

        person.gender = parseValeaGender(deelnemer.mv)
        boolean m = parseMembership(deelnemer)
        person.setMember(m)

        person.role = determinePersonRole(person)

        return person
    }

    Membership createMembership(Deelnemer deelnemer, Person person) {
        if (deelnemer.lid_start == null) {
            return null;
        }

        Membership membership = new Membership()
        membership.person = person
        membership.start = deelnemer.lid_start
        membership.end = deelnemer.lid_eind
        membership.setFormSigned(deelnemer.lidm_formulier_ontvangen)

        return membership
    }

    PersonRole determinePersonRole(Person person) {
        def ereleden = ["55","167","817","834","970","1824","5505","1889", "634"]
        if (ereleden.contains(person.plin)) {
            return PersonRole.HONORARY
        }

        def bestuursleden = [ "1193","3127", "6921", "892", "1286", "3179" ]
        if (bestuursleden.contains(person.plin)) {
            return PersonRole.MANAGEMENT
        }

        return PersonRole.REGULAR
    }

    boolean parseMembership(Deelnemer deelnemer) {
        if ( deelnemer.lid_start == null) {
            return false;
        }
        if (deelnemer.lid_eind != null) {
            return false;
        }
        return true;
    }

    Gender parseValeaGender(String input) {
        if (input == null || input.trim().isEmpty()) {
            return Gender.UNKNOWN
        }
        if (input.equalsIgnoreCase("m")) {
            return Gender.MALE
        }
        if (input.equalsIgnoreCase("v")) {
            return Gender.FEMALE
        }
        if (input.equalsIgnoreCase("f")) {
            return Gender.FEMALE
        }
        return Gender.UNKNOWN
    }

    boolean wantToMigrate(Deelnemer deelnemer) {
        if (deelnemer.plin == null || deelnemer.plin.trim().isEmpty()) {
            return false;
        }
        int plin = Integer.parseInt(deelnemer.plin)
        return plin <= 9999;
    }
}
