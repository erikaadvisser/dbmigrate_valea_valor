package nl.va.valeamigrate.steps

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.valea.Kas_boeking
import nl.va.valeamigrate.valea.ValeaBoekingMapping
import nl.va.valeamigrate.valea.ValeaEvent
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.valor.Payment
import org.va.valor.PaymentMatch
import org.va.valor.Purchase

/**
 * This class migrates Valea Boeking_events into Valor Matches and associated purchases
 */
@Component
class BoekingEventsMigrator {
    def static log = LoggerFactory.getLogger(BoekingEventsMigrator)

    @Autowired ValorRepo valorRepo

    int migratedMembershipMappings = 0
    int migratedOtherMappings = 0

    void run() {
        long startTime = System.currentTimeMillis()
        List<Integer> ids = Kas_boeking.listIdsfrom2013Onwards()

        log.info("Migrating ${ids.size()} persons.")
        ids.each({migrateForBoeking(it)})

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Migrated ${migratedMembershipMappings} boeking_event records for memberships.")
        log.info("Migrated ${migratedOtherMappings} boeking_event records for other.")
        log.info("duration: ${migrateTime} s")
    }

    void migrateForBoeking(long valeaKasBoekingId) {
        def valeaMappings = ValeaBoekingMapping.findByKasBoekingId(valeaKasBoekingId)
        valeaMappings.each({migrateMapping(it)})
    }

    def migrateMapping(ValeaBoekingMapping valeaBoekingMapping) {
        ValeaEvent valeaEvent = fetchValeaEvent(valeaBoekingMapping)

        if (!valorRepo.existsPaymentWithValeaId(valeaBoekingMapping.boekingId)) {
            log.debug("Payment does not exist: Failed to migrate mapping valea boeking_event.eventId= ${valeaBoekingMapping.eventId} boeking_event.boekingId= ${valeaBoekingMapping.boekingId}")
            Kas_boeking boeking = Kas_boeking.findById(valeaBoekingMapping.boekingId)
            log.debug("kas_boeking: id= ${boeking.id} ${boeking.bedrag} euro, date= ${boeking.datum}")
            return
        }
        Payment payment = valorRepo.fetchPaymentByValeaId(valeaBoekingMapping.boekingId)
        Purchase purchase = valorRepo.fetchPurchaseByValeaId(valeaEvent.id)

        PaymentMatch match = new PaymentMatch()
        match.amount = valeaBoekingMapping.bedrag;
        match.payment = payment
        match.purchase = purchase
        match.save()

        payment.amountOpen -= match.amount
        if (payment.amountOpen < 0) {
            log.warn("Inconsistency detected in Valea: matched amount > payment.amount_open for " +
                    "valea boeking_event.eventId= ${valeaBoekingMapping.eventId} boeking_event.boekingId= ${valeaBoekingMapping.boekingId} and payment.id= ${payment.id}")
        }
        valorRepo.updatePaymentAmountOpen(payment)

        // for membership events the purchase is meaningfull, i.e. we know what the person bought.
        // for entry purchases we don't know this yet, the purchase is for the temp product without a price.
        boolean isMembership = EventsMigrator.isMembershipEvent(valeaEvent);
        if (isMembership) {
            migratedMembershipMappings += 1
            purchase.amountOpen -= match.amount
            if (purchase.amountOpen < 0) {
                log.warn("Inconsistency detected in Valea: matched amount (${match.amount}) > amount open (${purchase.amountOpen + match.amount}) for valea boeking_event.eventId= ${valeaBoekingMapping.eventId} boeking_event.boekingId= ${valeaBoekingMapping.boekingId} and purchase.id= ${purchase.id}")
            }
            valorRepo.updatePurchaseAmountOpen(purchase)
        }
        else {
            migratedOtherMappings += 1
        }
    }

    ValeaEvent fetchValeaEvent(ValeaBoekingMapping valeaBoekingMapping) {
        long valeaEventId = valeaBoekingMapping.eventId
        return ValeaEvent.findById(valeaEventId)
    }
}
